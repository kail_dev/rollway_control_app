﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Rollway_Control_app
{

    public  class MainWindowViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Eanreader> ean;

      

        public ObservableCollection<Eanreader> Ean
        {
            get => ean; set
            {
                ean = value;
                OnPropertyChanged();
            }
        }

       
        public void RefreshEans()
        {
              sql aa = new sql();
            Ean = new ObservableCollection<Eanreader>(aa.GetEans());
        }

      
       private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        }

      

        public event PropertyChangedEventHandler PropertyChanged;

    }

}
