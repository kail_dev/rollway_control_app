﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rollway_Control_app
{

    [Table("ZZSPE_ROLLWAY_DOK", Schema = "cdn")]
    public  class Eanreader 
    {

        [Key]
        [Column("RD_ID")]
        public int Id { get; set; }

        [Column("RD_PLC_DOKID")]
        public int PLC_DOKID { get; set; }

        [Column("RD_EANREAD")]
        public string EANREAD { get; set; }
    }
}
